# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import codecs
import logging.config
import os.path
import sys
from dscoppelletti_textdump.textdump import TextDump

logger = logging.getLogger(__name__)

def main():
    args = _parse_args()

    if args.logging_conf_file:
        logging.config.fileConfig(args.logging_conf_file,
            disable_existing_loggers = False)
    else:
        logging.basicConfig()

    logger.debug('TextDump starts.')

    f_in = None
    f_out = None

    try:
        if args.overwrite and not args.output_file:
            raise ValueError(
                'Option --overwrite is invalid without option --output.')
        if not args.input_file:
            f_in = sys.stdin
        else:
            f_in = codecs.open(args.input_file, encoding = 'utf-8',
                mode = 'r')
        if not args.output_file:
            f_out = sys.stdout
        elif not args.overwrite and os.path.isfile(args.output_file):
            raise IOError('File {0} already exists.'.format(args.output_file))
        else:
            f_out = codecs.open(args.output_file, encoding = 'utf-8',
                mode = 'w')

        proc = TextDump(f_in, f_out)
        proc()
    except:
        logger.exception('TextDump failed.')
    finally:
        if f_in and args.input_file:
            f_in.close()
        if f_out and args.output_file:
            f_out.close()

    logger.debug('TextDump finished.')
    logging.shutdown()

def _parse_args():
    arg_parser = argparse.ArgumentParser(description = 'Dumps a file.')
    arg_parser.add_argument('-i', '--input', dest = 'input_file',
        metavar = 'FILE', help = 'input file')
    arg_parser.add_argument('-o', '--output', dest = 'output_file',
        metavar = 'FILE', help = 'output file')
    arg_parser.add_argument('--overwrite', dest = 'overwrite',
        action = 'store_true', default = False,
        help = 'whether output file can be overwritten')
    arg_parser.add_argument('-l', '--logging', dest = 'logging_conf_file',
        metavar = 'FILE', help = 'logging configuration file')
    return arg_parser.parse_args()

if __name__ == '__main__':
    main()