# Copyright (C) 2018 Dario Scoppelletti, <http://www.scoppelletti.it/>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

class TextDump:

    def __init__(self, f_in, f_out):
        self._f_in = f_in
        self._f_out = f_out

    def __call__(self):
        count = 0
        for line in self._f_in:
            count += 1
            line = line.rstrip("\n")
            uline = unicode(line) # May be from STDIN
            self._print_line(uline, count)
            self._dump_line(uline, count)

    def _print_line(self, line, count):
        self._f_out.write("C%010d:" % count)
        for c in line:
            self._f_out.write(" %-6s" % c)
        self._f_out.write("\n")

    def _dump_line(self, line, count):
        self._f_out.write("X%010d:" % count)
        for c in line:
            self._f_out.write(" 0x%04X" % ord(c))
        self._f_out.write("\n")